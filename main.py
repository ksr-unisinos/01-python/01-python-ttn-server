import ttn
from csv import writer,reader
import base64

# dados da aplicação
app_id = "teste_arduino"
access_key = "ttn-account-v2.EqWCFJNBnmCp62c0x5rXJ2IpZAiKsj6lR6GxP_N3FSQ"

# arquivo de saida
file = 'output.csv'

# função para armazenar dados na tabela
def append_list_as_row(file_name, list_of_elem):
	# Open file in append mode
	with open(file_name, 'a+', newline='') as write_obj:
		# Create a writer object from csv module
		csv_writer = writer(write_obj)
		# Add contents of list as last row in the csv file
		csv_writer.writerow(list_of_elem)

# função de callback para ler os dados do servidor
def uplink_callback(msg, client):
	print("Received uplink from ", msg.dev_id)
	print(msg)
	msg2 = msg.metadata
	msg3 = msg2.gateways[0]
	payload = base64.b64decode(msg.payload_raw)

	append_list_as_row(file,[
		msg.dev_id,
		msg.counter,
		payload,
		msg2.time,
		msg2.coding_rate,
		msg3.rssi,
		msg3.snr,
		msg3.timestamp
		])

# cria a chave
handler = ttn.HandlerClient(app_id, access_key)

# using mqtt client
mqtt_client = handler.data()
mqtt_client.set_uplink_callback(uplink_callback)
mqtt_client.connect()

# Captura uma tecla para sair
x = ''
while x != 'q':
	print('Digite q para sair')
	x = input('').split(" ")[0]

# fecha a conexão com o servidor
mqtt_client.close()



#Exemplo de mensagem
'''
Received uplink from  tinkerman01
MSG(
	app_id='teste_arduino', 
	dev_id='tinkerman01', 
	hardware_serial='A8610A30372B6708', 
	port=2, 
	counter=122, 
	confirmed=True, 
	payload_raw='MTEw', 
	metadata=MSG(
		time='2020-07-10T01:05:49.862009966Z', 
		frequency=903.9, 
		modulation='LORA', 
		data_rate='SF7BW125', 
		airtime=51456000, 
		coding_rate='4/5', 
		gateways=[MSG(
			gtw_id='rg1xx294549', 
			gtw_trusted=True, 
			timestamp=699315771, 
			time='', 
			channel=0, 
			rssi=-33, 
			snr=10, 
			rf_chain=0, 
			latitude=-30.025093, 
			longitude=-51.193874
		)]
	)
)
'''
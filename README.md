# 01-python-ttn-server

Este exemplo utiliza a biblioteca `ttn` para conectar ao servidor da rede The Things Network

para instalar:

```pip install ttn```

verifique mais informações de uso das chaves privadas de aplicação na página do próprio servidor

https://www.thethingsnetwork.org/docs/applications/python/

